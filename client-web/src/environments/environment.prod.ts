export const environment = {
  production: true,
  charteUrl: 'https://gitlab.com/api/v4/projects/8011854/repository/files/charte.md?ref=master',
  eventJson: 'assets/events.json',
};
