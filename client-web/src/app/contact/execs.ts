import { ExecInterface } from './exec.interface';

export const execsConst = [
  {
    position: 'président',
    fullname: 'Marianne Boyer',
    email: 'president@ageei.org',
    picture: 'assets/contact/execs/president.jpg',
    linkedin: 'boyer-marianne',
  },
  {
    position: 'secrétaire',
    fullname: 'Élodie Brunet',
    email: 'secretaire@ageei.org',
    picture: 'assets/contact/execs/secretaire.jpeg',
    github: 'croquette-Elo',
    linkedin: 'elodie-brunet-39635a204',
  },
  {
    position: 'trésorier',
    fullname: 'Jean-Christophe Clouâtre',
    email: 'tresorier@ageei.org',
    picture: 'assets/contact/execs/tresorier.jpg',
    linkedin: 'jean-christophe-c-b83a40262',
  },
  {
    position: 'vp-interne',
    fullname: 'Federico Barallobres',
    email: 'interne@ageei.org',
    picture: 'assets/contact/execs/interne.jpg',
  },
  {
    position: 'vp-externe',
    fullname: 'Émile Kolani',
    email: 'externe@ageei.org',
    picture: 'assets/contact/execs/externe.jpg',
    linkedin: 'émile-kolani-711720233',
  },
  {
    position: 'vp-loisir',
    fullname: 'Carl-William Bilodeau-Savaria',
    email: 'loisir@ageei.org',
    picture: 'assets/contact/execs/loisir.jpg',
  },
  {
    position: 'vp-techno',
    fullname: 'Sublime Tshimpangila',
    email: 'technologie@ageei.org',
    picture: 'assets/contact/execs/technologies.jpg',
    github: 'Sublime12',
    linkedin: 'sublime-tshimpangila-84aa78144',
  },
  {
    position: 'vp-compétition',
    fullname: 'Dany Gagnon',
    email: 'competition@ageei.org',
    picture: 'assets/contact/execs/competition.jpg',
    github: 'dgagn',
    linkedin: 'dany-g-34768822b',
  },
  {
    position: 'officier-premiere-annee',
    fullname: 'Badr Malouli',
    email: 'premiere_annee@ageei.org',
    picture: 'assets/contact/execs/officier_premier_annee.jpg',
    github: 'bmalouli',
    linkedin: 'badrmalouli',
  }
] as ExecInterface[];
