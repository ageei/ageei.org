export class ExecInterface {
  position: string;
  fullname: string;
  picture: string;
  email: string;
  linkedin?: string;
  github?: string;
}
