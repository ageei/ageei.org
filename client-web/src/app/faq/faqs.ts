import { FAQCategoryInterface } from './faq-category.interface';

export const faqs = [
  {
    categoryName: 'Photo de finissants',
    faqs: [
      {
        question: 'Quand ont lieux les photos de finissants ?',
        answer: 'À la fin de la session d’hiver (au mois d’avril).',
      },
      {
        question: 'Qui peut se faire photographier ?',
        answer:
          'Les étudiants et étudiantes ayant gradué au premier cycle lors de la session d’hiver ou les sessions d’automne et d’hiver précédent la session où les photos sont prises. Une preuve de votre graduation peut vous être demandée.',
      },
    ],
  },
  {
    categoryName: 'Party',
    faqs: [
      {
        question: 'Comment savoir quand et où ont lieu les party ?',
        answer:
          'Nous produisons généralement des affiches une semaine ou deux avant les partys, et faisons des annonces sur nos canaux officiels. Vous pouvez vous référer à la section Contacts.',
      },
      {
        question: 'Combien est-ce que ça coûte ?',
        answer: 'Ça dépend pour chaque party.',
      },
    ],
  },
  {
    categoryName: 'Conférences et présentations',
    faqs: [
      {
        question: 'Quand ont lieu les conférences organisées par l’AGEEI ?',
        answer:
          'Nous annoncerons les conférences environ une semaine avant qu’elles aient lieu sur nos plateformes officielles (vous pouvez vous référer à la section Contacts). Habituellement, les conférences que nous organisons sont le mercredi midi, pendant la pause commune.',
      },
      {
        question: 'Sur quels sujets portent les conférences ?',
        answer:
          'Nous essayons d’offrir des conférences qui portent sur des sujets variés qui touchent les étudiants et étudiantes. Par exemple, nous avons des conférences de professeurs sur leurs sujets de recherche, des étudiants qui présentent une nouvelle technologie, des entreprises qui présentent un enjeu ou une technologie sur laquelle ils travaillent.',
      },
      {
        question: 'Est-ce qu’un repas est fourni ?',
        answer: 'Oui ! Habituellement c’est de la pizza qui est au menu !',
      },
      {
        question: 'Combien est-ce que ça coûte ?',
        answer: 'Rien ! Nous finançons les conférences avec les cotisations de nos membres.',
      },
    ],
  },
  {
    categoryName: 'Association',
    faqs: [
      {
        question: 'Quelle est la différence entre l’AGEEI et l’AESS ?',
        answer:
          "L’AGEEI est l’Association Générale des Étudiants et Étudiantes en Informatique et l’AESS est l’Association Étudiante du Secteur des Sciences. Les missions de l’AGEEI et de l’AESS sont somme toute assez similaires. Par contre, l’AESS représente les membres de l’ensemble de la faculté des sciences de l’UQAM alors que l’AGEEI représente uniquement les membres des programmes de premier cycle de l’UQAM. Généralement, l’AGEEI offre des services plus personnalisés aux étudiants et étudiantes en informatique (party, conférences, initiations, photo de finissants) et l’AESS offre des services qui concernent toute la communauté étudiante (assurances, permanence, aide avec les plaintes d'étudiants et étudiantes, party). Il est aussi à noter que l’AESS et l’AGEEI ne sont pas gérées par les mêmes personnes.",
      },
      {
        question: 'Suis-je membre de l’AGEEI ?',
        answer:
          'Les personnes inscrites aux programmes suivants sont membres de l’AGEEI : Baccalauréat en informatique et génie logiciel (7316, 7416, 7616, 7617), Certificat en informatique et en développement de logiciels (4702). Si vous êtes inscrits aux cycles supérieurs en informatique, d’autres associations existent pour votre programme. Votre adhésion est automatiquement renouvelée chaque session.',
      },
      {
        question: 'Suis-je membre de l’AESS ?',
        answer:
          'Si vous étudiez dans l’un des programmes de la faculté des sciences de l’UQAM, vous êtes membre de l’AESS. Votre adhésion est automatiquement renouvelée chaque session. Pour plus de questions, vous pouvez communiquer directement avec l’AESS à l’adresse suivante [aess@aess.org](https://mailto:aess@aess.org), ou vous référer à leur site web : [aessuqam.org](https://aessuqam.org/).',
      },
      {
        question: 'Où est situé le local de l’AGEEI ?',
        answer:
          "Notre local est situé au SH-R345 du pavillon Sherbrooke de la faculté des sciences, tout juste avant l’entrée du café fractal. Nous avons mis beaucoup d'efforts dans les derniers mois pour le rendre accueillant pour ceux et celles qui souhaitent travailler sur leurs travaux ou simplement décompresser entre deux cours. Au plaisir de vous y voir !",
      },
      {
        question: 'Comment puis-je me retirer de l’AGEEI ?',
        answer:
          'Vous pouvez contacter l’AGEEI à l’adresse courriel suivante : [executif@ageei.org](mailto:executif@ageei.org). Un membre du comité exécutif devrait être en mesure d’effectuer votre remboursement. Notez qu’en étant plus membre de l’AGEEI, vous perdez vos privilèges liés à l’association pour la durée de la session pour laquelle vous demandez un remboursement. Notez que vous avez jusqu’à la deuxième semaine d’une session pour vous révoquer votre adhésion à l’AGEEI.',
      },
      {
        question: 'Je souhaite m’impliquer ! Comment puis-je faire ?',
        answer:
          'Il existe plusieurs façons de vous impliquer dans votre association. La plus évidente est de vous présenter pour devenir membre du comité exécutif. Vous pouvez consulter la charte de l’AGEEI ou discuter avec un membre de l’exécutif pour connaître les positions qui sont disponibles et qui pourraient vous intéresser. Des élections ont lieu chaque année au mois d’avril. Nous avons aussi souvent besoin de bénévoles pour les activités qui nécessitent davantage d’organisation, comme les initiations par exemple.',
      },
      {
        question: 'Qui sont les membres du comité exécutif ?',
        answer:
          'Les membres du comité exécutif sont inscrits en haut de la charte. Vous pouvez aussi les trouver dans la section Contact de notre site.',
      },
    ],
  },
  {
    categoryName: 'Assurances',
    faqs: [
      {
        question: 'Qu’est-ce que l’assurance inclue ?',
        answer:
          'Nous vous recommandons d’aller trouver ces informations sur le site de l’assureur : [aseq.ca](https://aseq.ca), en sélectionnant AESS comme association étudiante. Il existe une couverture de base et une couverture bonifiée.',
      },
      {
        question: 'Comment puis-je me retirer des assurances fournies par l’association ?',
        answer:
          'Sur le site web de l’assureur ([aseq.ca](https://aseq.ca)), vous pouvez sélectionner AESS. Une fois sur la page d’accueil, certaines informations utiles vous sont accessibles : la période de retrait des assurances et le retrait des assurances.',
      },
      {
        question: 'Quand puis-je retirer des assurances fournies par l’association ?',
        answer:
          'Généralement, il est possible de se retirer des assurances entre la mi-septembre et la mi-octobre. Les périodes de retrait exact sont accessibles sur le site web de l’assureur ([aseq.ca](https://aseq.ca)), en sélectionnant AESS comme association étudiante. Même si vous avez déjà payé vos frais d’assurance après avoir demandé un remboursement, vous serez soit crédité, soit remboursé.',
      },
    ],
  },
] as FAQCategoryInterface[];
